# Exercice : tableau, caractère et chaînes de caractères

En partant de texte.c et Makefile (à copier dans un repertoire
de votre dépôt GIT, add + commit + push) :

- Ajoutez du code afin de compter le nombre d'espaces dans la
  chaîne saisie et l'afficher à la fin.

- Quelle est la différence entre le code ASCII de 'A' et de 'a' ?
  De 'B' et de 'b', de 'M' et de 'n' ? Que concluez-vous ?

- Ajoutez du code qui modifie la chaîne `texte` et passe tous
  les caractères en majuscule s'il sont en minuscule et 
  affiche la nouvelle chaîne. NB : sans utiliser de fonction
  de la libc (pas de `string.h`)


