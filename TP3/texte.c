#include<stdio.h>
#include<stdlib.h>

int main() {
    char texte[100]; // chaîne de taille <= 999 (zéro terminal !)
    int i; 

    printf("Tapez une phrase : "); 
    scanf("%[^\n]", texte);
    printf("Phrase : %s\n", texte);

    i = 0;
    while (texte[i] != '\0') { // ou != 0
       printf("%c ", texte[i]);
       i++;
    }
    printf("\nLongueur de la chaîne : %d\n", i);
    exit(0);
}
